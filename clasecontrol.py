import matplotlib.pyplot as plt
import numpy as np
import mpmath as mp

from sympy import *
init_printing()

# Definición de variables
s = symbols("s")
t, Kp, Tp, Kc, Ti, Td, w = symbols("t K_p tau_p K_c tau_I tau_d omega",
                                   real=True)
plt.rcParams.update({'font.size': 12})


def modoclase(clase):
    '''
    Cambia el tamaño de la fuente de los gráficos para que se vean mejor en
    clase.

    modoclase(True): Letra grande
    modoclase(False): Letra pequeña
    '''
    if clase:
        plt.rcParams.update({'font.size': 18})
    else:
        plt.rcParams.update({'font.size': 12})

def simul(f, tmin=0.01, tmax=10, n=100, simplot = True):
    '''
    Calcula la transformada inversa de laplace numéricamente y dibuja la
    respuesta:

    Argumentos:
    tmin: Tiempo mínimo (debe ser >0)
    tmax: Tiempo máximo a simular
    n: Número de puntos a calcular (por defecto 100)
    simplot: True, dibuja la respuesta. False, no plot

    Ejemplos:
    tt, yy = simul(1/(s+1)*1/s*exp(-s), 0.1,16,50)
    t2, y2 = simul(20/(2*s**2+6*s+20)*1/s, 0.01,10)
    '''
    mp.dps = 8
    
    # Si se toma un valor de tmin = 0, que generara
    # error, se sustituye por uno que se estima suficientemente
    # pequeño
    if tmin == 0:
        tmin = 0.01

    # Creamos una función que puede manejar mpmath, ya que no puede
    # trabajar directamente con una expresión de sympy
    fp = lambdify(s, f, 'mpmath')

    # Vector con los tiempos a simular
    t = np.linspace(tmin, tmax, n)

    # Genera un vector y con la transformada inversa de Laplace numérica
    y = [mp.invertlaplace(fp, i, method="dehoog") for i in t]
    if simplot:
        plt.plot(t, y)
        plt.xlabel("t")
        plt.ylabel("y")
    return np.array(t), np.array(y)

def bode2(Gol, winf=0.1, wsup=10, co=False):
    '''
    Dibuja el diagrama de Bode numericamente:

    Argumentos:
    Gol: Función de transferencia de lazo abierto
    winf: Frecuencia inferior para representar
    wsup: Frecuencia superior
    co: Para calcular y representar la frecuencia de cruce y RA(wco)

    Salida (solo caundo 'co = True':
    wco: Frecuencia de cruce
    RAco: Valor de la razón de amplitudes para la frecuencia de cruce,
          RA(wco)

    Ejemplos:
    wco, RAco = bode2(1/(s**2+0.5*s+1)*2*(1+1/s)*1/(s+1)*exp(-2*s),
                      0.1, 10, True)
    bode2(-0.1*(s+400)/((s+20)*(s+2000)), 1, 10**5)
    '''
    if type(Gol) is list:
        wcol = []
        RAcol = []
        for i in Gol:
            if co:
                wcoi, RAcoi = bode2(i, winf, wsup, co)
                wcol.append(wcoi)
                RAcol.append(RAcoi)
            else:
                bode2(i, winf, wsup, co)
        if co:
            return wcol, RAcol
    else:
        # Realizamos la sustitución s = i*w para poder calcular el desfase
        # y la razón de amplitudes
        Gw = Gol.subs({s:I*w})

        # Cálculo de la función de RA y del desfase
        RA = sqrt(re(Gw)**2+im(Gw)**2)
        phi = arg(Gw)

        # Valores de frecuencia para los que representaremos el diagrama de
        # Bode
        # Calculamos el logaritmo para que queden equiespcaciados en la
        # escala logarítmica
        wplot = np.logspace(np.log10(winf), np.log10(wsup))

        # Calculamos los valores de RA que vamos a representar
        RAplot2 = [RA.subs({w:i}) for i in wplot]

        # Dibujaremos el diagrama de Bode como 2 subplots en una columna
        # (2,1)
        fig, (RA_plot, phi_plot) = plt.subplots(2, 1, figsize=(7, 7))

        # Dibujo del diagrama de razón de amplitudes
        RA_plot.plot(wplot, RAplot2)
        RA_plot.set_xscale('log')
        RA_plot.set_yscale('log')
        RA_plot.set_ylabel("RA")
        RA_plot.grid(which='both')

        # En el diagrama de fase existe el problema causado por el atan, se
        # resetea y quedan unos ciclos muy feos, por ejemplo, en el
        # retraso.
        # Para evitarlo detectaremos cuando el desfase aumenta de valor muy
        # rapidamente, es decir, cuando su derivada toma un valor elevado.
        # En ese caso, añadiremos un ciclo

        n = [0]  ## Cuantas veces se completa un ciclo

        # Calculamos el desfase a partir de la fórmula
        phiplot = [phi.subs({w:i})*180/np.pi for i in wplot]

        # Calcularemos la derivada para cada punto, si el valor es elevado
        # añadiremos un ciclo (360°). El número de vueltas es una lista (n) con el
        # número de ciclos asociado a cada frecuencia
        contador = 0
        for i in np.diff(phiplot, 1):
            if i > 100:
                contador += 1
            n.append(contador)

        # Transformamos la lista en un array de numpy para poder operar
        # fácilmente
        n = np.array(n)

        # Cálculo del desfase teniendo en cuenta el número de ciclos
        phiplot2 = phiplot - n*360

        # Representación del desfase
        phi_plot.plot(wplot, phiplot2)
        phi_plot.set_xscale('log')
        phi_plot.set_xlabel("$\omega$")
        phi_plot.set_ylabel("$\phi$")
        phi_plot.grid(which='both')
        plt.subplots_adjust(hspace=.0)

        # Cálculo de la frecuencia de cruce (wco) y de RA para la
        # frecuencia de cruce (RA(wco) = Raco)
        if co:
            # Comprobamos que la frecuencia de cruce exista en el diagrama
            # de Bode
            if max(phiplot2) > -180 and min(phiplot2) < -180:
                # Calcula la frecuencia de cruce interponlando entre los
                # valores de desfase calculados
                wco = np.interp(180, -phiplot2.astype(float), wplot)

                # Cálculo de RA(wco)
                RAco = RA.subs({w:wco})

                # Añade las líneas con los valores de wco y RAco al
                # diagrama de Bode
                RA_plot.plot([RA_plot.get_xlim()[0], wco], [RAco, RAco],
                             "r-.")
                RA_plot.plot([wco, wco], [RA_plot.get_ylim()[0], RAco],
                             "r-.")
                RA_plot.annotate("  $RA_{co}$ = %.4f"%RAco,
                                 [RA_plot.get_xlim()[0], RAco], va="top")
                phi_plot.plot([wco, wco], [phi_plot.get_ylim()[0],
                              phi_plot.get_ylim()[1]], "r-.")
                phi_plot.plot([phi_plot.get_xlim()[0], wco], [-180, -180],
                              "r-.")
                phi_plot.annotate("$\omega_{co}$ = %.4f "%wco,
                                  [wco, phi_plot.get_ylim()[0]],
                                  ha="right", va="bottom")
                phi_plot.annotate("  -180°",
                                  [phi_plot.get_xlim()[0], -180],
                                  va="bottom")
                return wco, RAco
            else:
                print("No hay wco en el rango de frecuencias.")
                return np.nan, np.nan

def step(g, tmin=0.01, tmax=10, n=100):
    '''
    Representa la respuesta de un proceso para una entrada en escalón
    unidad.
    '''
    f = 1/s
    if type(g) is list:
        for i in g:
            simul(i*f, tmin, tmax, n)
    else:
        simul(g*f, tmin, tmax, n)

def impulse(g, tmin=0.01, tmax=10, n=100):
    '''
    Representa la respuesta de un proceso para un impulso unidad.
    '''
    if type(g) is list:
        for i in g:
            simul(i, tmin, tmax, n)
    else:
        simul(g, tmin, tmax, n)


def closed_loop(gol):
    '''
    Calcula la función de trasferencia de lazo cerrado a partir de la
    función de trasnferencia de lazo abierto.
    '''
    if type(gol) is list:
        gcl = []
        for i in gol:
            gcl.append(scf(i/(1+i)))
        return gcl
    else:
        return scf(gol/(1+gol))

def open_loop(gcl):
    '''
    Devuelve la ecuación característica a partir de la función de
    trasnferencia de lazo cerrado.
    '''
    if type(gcl) is list:
        gol = []
        for i in gcl:
            gol.append(open_loop(i))
        return gol
    else:
        #den = fraction(gcl)[1]
        #den1 = den/den.subs({s:0})
        #return scf(1/(den1-1))
        return scf(gcl/(1-gcl))

def sinresp(g, w=1, tmin=0.01, tmax=20, n=100):
    '''
    Respuesta de un proceso a una entrada sinusoidal de frecuencia omega.
    '''
    f = w/(w**2+s**2)
    if type(g) is list:
        for i in g:
            simul(i*f, tmin, tmax, n)
    else:
        simul(g*f, tmin, tmax, n)

def scf(g):
    '''
    Toma factor común de la variable 's'.
    '''
    if type(g) is list:
        glist = []
        for i in g:
            glist.append(collect(factor(simplify(i)), s))
        return glist
    else:
        return collect(factor(simplify(g)), s)

def ise(y, g, tmin = 0.01, tmax = 10, n = 100):
    '''
    Calcula la integral del error al cuadrado (ISE) numericamente.
    
    Agumentos:
    y: Función de entrada, y(s)
    g: Función de transferencia, típicamente la del lazo de control
    tmin: Tiempo mínimo, valor cercano a 0.
    tmax: Tiempo máximo a simular.
    n: Número de puntos de la simulación
    '''
    tt, error = simul(y*(1-g), tmin, tmax, n, simplot = False)
    ise = np.trapz(error**2, tt)
    return ise